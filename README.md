# Test Artifacts

Browser les artifacts :
https://gitlab.com/gsalaun1/test-artifact/-/jobs/artifacts/master/browse?job=build

Télécharger les artifacts :
https://gitlab.com/gsalaun1/test-artifact/-/jobs/artifacts/master/download?job=build

Accéder à un fichier spécifique :
https://gitlab.com/gsalaun1/test-artifact/-/jobs/artifacts/master/raw/test-artifact-latest.jar?job=build

## Lancer l'application

### Pré-requis
- wget : téléchargement de la dernière version
- Java 11 : exécution de l'application

### Commande

```shell
run.sh
```
